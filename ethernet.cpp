#include "ethernet.h"

namespace noname_packet_dump 
{
	mac_address::mac_address() : address{ 0 } {  }

	mac_address::mac_address(const uint8_t* data) : address{ 0 }
	{
		for (int i = 0; i < LEN; ++i)
			address[i] = data[i];
	}

	mac_address::mac_address(const mac_address& m) : address{ 0 }
	{
		for (int i = 0; i < LEN; ++i)
			address[i] = m.address[i];
	}

	std::string mac_address::to_string(const char delimiter=':') const
	{
		char str[256];
		sprintf(str, "%02X%c%02X%c%02X%c%02X%c%02X%c%02X"
			, address[0], delimiter
			, address[1], delimiter
			, address[2], delimiter
			, address[3], delimiter
			, address[4], delimiter
			, address[5]
		);
		return std::string(str);
	}

	mac_address& mac_address::operator=(const mac_address& m)
	{
		for (int i = 0; i < LEN; ++i)
			this->address[i] = m.address[i];
		return *this;
	}

	bool mac_address::operator==(const mac_address& m)
	{
		for (int i = 0; i < LEN; ++i)
			if (address[i] != m.address[i]) return false;
		return true;
	}

	bool mac_address::operator!=(const mac_address& m)
	{
		return !(*this == m);
	}

	std::ostream& operator<<(std::ostream& os, const mac_address& m)
	{
		os << m.to_string();
		return os;
	}

	ethernet_header::ethernet_header()
		: destination()
		, source()
		, ether_type(0) { }

	ethernet_header::ethernet_header(const uint8_t* data)
		: destination(data)
		, source(data + mac_address::LEN)
		, ether_type(*(data + 2 * mac_address::LEN)) { }

	ethernet_header::ethernet_header(const ethernet_header& e)
		: destination(e.destination)
		, source(e.source)
		, ether_type(e.ether_type) { }

	void ethernet_header::set_destination(mac_address destination) { this->destination = destination; }
	void ethernet_header::set_source(mac_address source) { this->source = source; }
	void ethernet_header::set_ether_type(uint16_t ether_type) { this->ether_type = ether_type; }

	mac_address ethernet_header::get_destination() const { return destination; }
	mac_address ethernet_header::get_source() const { return source; }
	uint16_t ethernet_header::get_ether_type() const { return ether_type; }

	std::string ethernet_header::to_string() const
	{
		std::ostringstream ss;

		ss << "Destination mac address: " << destination << std::endl
			<< "Source mac address: " << source << std::endl;

		return ss.str();
	}

	PacketType ethernet_header::get_next_packet_type() const
	{
		switch (ether_type)
		{
		case ETHER_TYPE_IP:
			return PacketType::IP;
		case ETHER_TYPE_ARP:
			return PacketType::ARP;
		default:
			break;
		}
		return PacketType::UNKNOWN;
	}

	ethernet_header& ethernet_header::operator=(const ethernet_header& e)
	{
		destination = e.destination;
		source = e.source;
		ether_type = e.ether_type;
		return *this;
	}

	bool ethernet_header::operator==(const ethernet_header& m)
	{
		return destination == m.destination
			&& source == m.source
			&& ether_type == m.ether_type;
	}

	bool ethernet_header::operator!=(const ethernet_header& m)
	{
		return !(*this == m);
	}

	std::ostream& operator<<(std::ostream& os, const ethernet_header& e)
	{
		os << e.to_string();
		return os;
	}
}