#include "DumpManager.h"

namespace noname_packet_dump
{
	Dump_manager::Dump_manager()
		: status(STATUS::OFFLINE)
		, handle(nullptr)
		, errbuf{ 0 } { }

	Dump_manager::Dump_manager(const char* dev_name)
		: errbuf{ 0 } 
	{ 
		connect_to_pcap(dev_name);
	}

	void Dump_manager::connect_to_pcap(const char* dev_name)
	{
		handle = pcap_open_live(dev_name, BUFSIZ, 1, 1000, errbuf);
		if (handle == NULL) {
			fprintf(stderr, "couldn't open device %s: %s\n", dev_name, errbuf);
			status = STATUS::OFFLINE;
		}
		status = STATUS::ONLINE;
	}

	pcap_t* Dump_manager::get_handle() const
	{
		return handle ? handle : nullptr;
	}

	int Dump_manager::get_next_packet(Packet* next_packet)
	{
		int res = pcap_next_ex(handle, &next_packet->p_header, &next_packet->packet);
		next_packet->init();

		return res;
	}

}