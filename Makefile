all : packetDump

packetDump: main.o
	g++ -g -o packetDump main.o L2Ptype.o L3Ptype.o L4Ptype.o DumpManager.o Packet.o -lpcap

main.o: L2Ptype.o L3Ptype.o L4Ptype.o DumpManager.o Packet.o nonamePdump.h L2Ptype.h L3Ptype.h L4Ptype.h DumpManager.h Packet.h main.cpp
	g++ -g -c -o main.o main.cpp

L2Ptype.o: L2Ptype.cpp L2Ptype.h nonamePdump.h
	g++ -g -c -o L2Ptype.o L2Ptype.cpp

L3Ptype.o: L3Ptype.cpp L3Ptype.h nonamePdump.h
	g++ -g -c -o L3Ptype.o L3Ptype.cpp

L4Ptype.o: L4Ptype.cpp L4Ptype.h nonamePdump.h
	g++ -g -c -o L4Ptype.o L4Ptype.cpp

DumpManager.o: DumpManager.cpp DumpManager.h nonamePdump.h
	g++ -g -c -o DumpManager.o DumpManager.cpp

Packet.o: Packet.cpp Packet.h nonamePdump.h
	g++ -g -c -o Packet.o Packet.cpp

clean:
	rm -f packetDump 
	rm -f *.o
