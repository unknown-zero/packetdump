#pragma once

#include "t_PacketDump.h"

namespace noname_packet_dump 
{
	enum class PacketType {
		Ethernet,
		ARP,
		IP,
		TCP,
		UDP,
		HTTP,
		UNKNOWN
	};

	struct header { };
}